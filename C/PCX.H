/*
 - VESA LIBRARY SOURCE V2.0 by Babyloon/Revelation -

 this file is used while compiling C source..
 It comes from the RVL-VBE20.ZIP package available since Jully 1997.
 ----------------------------------------------------------------------------
 All this code have been typed by babyloon/revelation
 use it where you want, for what you want, it is at you own risk !
*/

long fsize(FILE *f) {
  long pos,length;

  pos=ftell(f);
  fseek(f,0L,SEEK_END);
  length=ftell(f);
  fseek(f,pos,SEEK_SET);
  return length;
}


/*
 void unpackpcx(char *name,char *picture,long size);
 This function runs only with 8bits pictures ..
*/
void unpackpcx(char *name,char *destination,long size) {
  FILE *f;
  long filesize;
  char *buffer;
  long bufferpos=0,picturepos=0,index;
  char pixel,value;

  f=fopen(name,"rb");
  filesize=fsize(f);
  buffer=(char *)malloc(sizeof(char)*filesize);
  fread(buffer,sizeof(char),filesize,f);
  fclose(f);

  bufferpos=128;
  while(picturepos<size) {
    if((pixel=*(buffer+bufferpos))<=192) {
      *(destination+picturepos)=pixel;
      picturepos++;
      bufferpos++;
    } else {
      pixel=pixel-192;
      value=*(buffer+bufferpos+1);
      for(index=0;index<pixel;index++)
        *(destination+picturepos+index)=value;
      picturepos+=pixel;
      bufferpos+=2;
    }
  }

  bufferpos=filesize-768;
  for(index=0;index<256;index++) {
    outp(0x3C8,(int)index);
    value=(buffer[bufferpos]>>2);
    outp(0x3C9,value);
    value=(buffer[bufferpos+1]>>2);
    outp(0x3C9,value);
    value=(buffer[bufferpos+2]>>2);
    outp(0x3C9,value);
    bufferpos=bufferpos+3;
  }

  free(buffer);
}
